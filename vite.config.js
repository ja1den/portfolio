import { resolve } from "path";

import siimple from "@siimple/postcss";
import { defineConfig } from "vite";

export default defineConfig({
  appType: "mpa",
  build: {
    emptyOutDir: true,
    outDir: "../dist/",
    rollupOptions: {
      input: [
        resolve(__dirname, "src", "index.html"),
        resolve(__dirname, "src", "projects", "booklet", "index.html"),
        resolve(__dirname, "src", "projects", "dash", "index.html"),
        resolve(__dirname, "src", "projects", "pedalprix", "index.html"),
        resolve(__dirname, "src", "projects", "pointbase", "index.html"),
      ],
    },
  },
  css: {
    postcss: {
      plugins: [siimple("./siimple.config.js")],
    },
  },
  publicDir: "../public/",
  root: "./src/",
});
