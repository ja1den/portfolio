# Home

> My portfolio.

## Introduction

This repository houses the source code for my portfolio.

It's built with vanilla HTML, CSS, and JavaScript, and is bundled with [Vite](https://vitejs.dev/).

## Contributing

Clone the repository:

```sh
git clone git@gitlab.com:ja1den/home.git
```

Install the dependencies with [`npm`](https://npmjs.com/):

```sh
npm install
```

### Development

Start the [Vite](https://vitejs.dev/) development server:

```sh
npm run dev
```

Then, open your browser to http://localhost:5173/.

Lint the project with [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/).

```
npm run lint
```

### Production

Generate a production build of the website:

```sh
npm run build
```

Preview the production build locally:

```sh
npm run preview
```

Host the production with [`serve`](https://npmjs.com/package/serve):

```sh
npm start
```

## Credits

I found Josh Comeau's [_How to Build an Effective Dev Portfolio_](https://joshwcomeau.com/effective-portfolio/) an invaluable resource while building my portfolio.\
I highly recommend you check it out, especially if you're trying to do something similar.
