import { createIcons } from "lucide";
import { Github, Gitlab, Mail } from "lucide";

import "./index.css";

createIcons({ icons: { Github, Gitlab, Mail } });
